package com.minderaschool.Temperature;

import java.util.List;

public class City {

    private List<String> nameCity;
    private List<Integer> latitude;
    private List<Integer> longitude;
    private List<String> country;

    public City(List<String> nameCity, List<Integer> latitude, List<Integer> longitude, List<String> country) {
        this.nameCity = nameCity;
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
    }

    public List<Integer> getLatitude() {
        return latitude;
    }

    public List<Integer> getLongitude() {
        return longitude;
    }

    public List<String> getCountry() {
        return country;
    }

    public void setNameCity(List<String> nameCity) {
        this.nameCity = nameCity;
    }

    public void add(List<Integer> latitude, List<Integer> longitude){

    }
}
