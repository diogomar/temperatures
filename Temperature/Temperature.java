package com.minderaschool.Temperature;

import java.util.List;

public class Temperature {

    private List<City> cidades;
    private List<Days>dias;
    private int time ;
    private String celcius;
    public int temp;

    public Temperature(List<City> cidades, List<Days> dias, int time, String celcius, int temp) {
        this.cidades = cidades;
        this.dias = dias;
        this.time = time;
        this.celcius = celcius;
        this.temp = temp;
    }

    public List<City> getCidades() {
        return cidades;
    }

    public List<Days> getDias() {
        return dias;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setCelcius(String celcius) {
        this.celcius = celcius;
    }


}
