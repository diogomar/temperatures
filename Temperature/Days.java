package com.minderaschool.Temperature;

import java.util.List;

public class Days {

    private List<Temperature> temperatures;
    private List<Integer> hightTemperatures;
    private List<Days> dias;
    private List<City> cidades;

    private int hours;
    private int minutes;
    private int seconds;
    private int month;


    public Days(List<Temperature> temperatures, List<Integer> hightTemperatures, List<Days> dias, List<City> cidades, int hours, int minutes, int seconds, int month) {
        this.temperatures = temperatures;
        this.hightTemperatures = hightTemperatures;
        this.dias = dias;
        this.cidades = cidades;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.month = month;
    }

    public List<Days> getDias() {
        return dias;
    }

    public List<City> getCidades() {
        return cidades;
    }

    public List<Temperature> getTemperatures() {
        return temperatures;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public void setMonth(int month) {
        this.month = month;
    }


}
