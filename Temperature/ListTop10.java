package com.minderaschool.Temperature;

import java.util.List;

public class ListTop10 {

    private List<Temperature> temperatures;
    private List<City> cidades;
    private List<ListTop10> top10s;

    public ListTop10(List<Temperature> temperatures, List<City> cidades, List<ListTop10> top10s) {
        this.temperatures = temperatures;
        this.cidades = cidades;
        this.top10s = top10s;
    }

    public List<Temperature> getTemperatures() {
        return temperatures;
    }

    public List<City> getCidades() {
        return cidades;
    }

    public List<ListTop10> getTop10s() {
        return top10s;
    }

    private void setTop10s(List<ListTop10> top10s) {
        this.top10s = top10s;
    }
}
